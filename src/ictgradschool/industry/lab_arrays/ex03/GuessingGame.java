package ictgradschool.industry.lab_arrays.ex03;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {

        // TODO Write your code here.
        int goal =(int)Math.ceil(100*Math.random());
        int guess = 0;
        //System.out.println("Cheat and print goal: " + goal);
        while(guess !=goal){
            System.out.println("Guess a number between 1 and 100");
            guess = Integer.parseInt(Keyboard.readInput());
            if(guess>goal){
                System.out.println("Too high, try again");
            }
            else if(guess<goal){
                System.out.println("Too low, try again");
            }
            else{
                System.out.println("Perfect!");
            }
        }
        System.out.println("Goodbye");


    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
