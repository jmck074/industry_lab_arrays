package ictgradschool.industry.lab_arrays.ex06;

public class MobilePhone {

    // TODO Declare the 3 instance variables:
    private String brand;
    private String model;
    private double price;
    
    public MobilePhone(String brand, String model, double price) {
        this.brand = brand;
        this.model = model;
        this.price = price;
    }

    // TODO Uncomment these methods once the corresponding instance variable has been declared.
    public String getBrand() {
        return brand;
    }
    
    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    // TODO Insert getModel() method here
    public String getModel(){
        return model;
    }
    // TODO Insert setModel() method here
    public void setModel(String model){
        this.model = model;
    }
    // TODO Insert getPrice() method here
    public double getPrice(){
        return price;
    }
    // TODO Insert setPrice() method here
    public void setPrice(double price){
        this.price = price;
    }
    // TODO Insert toString() method here
    public String toString(){
        String str = brand+ " " + model + " which cost $" + price;
        return str;
    }
    // TODO Insert isCheaperThan() method here
    public boolean isCheaperThan(MobilePhone otherPhone){
        boolean priceTest = price<otherPhone.price;
        return priceTest;
    }
    // TODO Insert equals() method here
    public boolean equals(MobilePhone otherPhone){
        boolean areSame = model.equals(otherPhone.model);
        return areSame;
    }
}


