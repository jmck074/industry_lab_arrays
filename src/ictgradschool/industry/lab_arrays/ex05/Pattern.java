package ictgradschool.industry.lab_arrays.ex05;

public class Pattern {
    //private String sequence;
    private int reps;
    private char symbol;

    public Pattern(int reps, char symbol) {
        this.reps = reps;
        this.symbol = symbol;

        }


    public String toString() {
        String sequence = "";
        for (int i = 0; i < reps; i++) {
            sequence = sequence + symbol;

        }
        return sequence;
    }
    public void setNumberOfCharacters(int newReps){
        reps = newReps;
    }
    public int getNumberOfCharacters(){
        return reps;

    }
}
