package ictgradschool.industry.lab_arrays.ex04;

import com.sun.org.apache.xpath.internal.SourceTree;
import com.sun.xml.internal.ws.message.jaxb.JAXBMessage;
import ictgradschool.Keyboard;

/**
 * A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.
    public static final int SCISSORS = 2;
    public static final int PAPER = 3;
    public boolean keepPlaying = true;

    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.
        String playerName = getUserName();
        while(keepPlaying){
            int userChoice = getUserChoice();
            if (userChoice == 4) {
                keepPlaying = false;
                break;
            }
            int computerChoice = getComputerChoice();
            displayPlayerChoice(playerName,userChoice);
            displayPlayerChoice("Computer", computerChoice);
            System.out.println(getResultString(userChoice,computerChoice));
        }
        System.out.println("Goodbye "+playerName+". Thanks for playing :)");
    }

    public String getUserName() {
        System.out.println("Please enter the player name: ");
        String playerName = Keyboard.readInput();
        return playerName;
    }

    public int getUserChoice() {
        System.out.println("");
        System.out.println("1. Rock");
        System.out.println("2. Scissors");
        System.out.println("3. Paper");
        System.out.println("4. Quit");
        System.out.println("Enter choice: ");
        int userChoice = Integer.parseInt(Keyboard.readInput());


        return userChoice;

    }
    public int getComputerChoice(){
        int computerChoice= (int)Math.ceil(Math.random()*3);
        return computerChoice;
    }

        public void displayPlayerChoice (String name,int choice){
            // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)

            String[] weapons = new String[]{"rock", "scissors", "paper"};
            System.out.println(name + " chose " + weapons[choice - 1] + ".");
        }

        public boolean userWins ( int playerChoice, int computerChoice){
            // TODO Determine who wins and return true if the player won, false otherwise.
            if ((playerChoice == 1 && computerChoice == 2) || (playerChoice == 2 && computerChoice == 3) || (playerChoice == 3 && computerChoice == 1)) {
                return true;
            }
            return false;
        }

        public String getResultString ( int playerChoice, int computerChoice){

            final String PAPER_WINS = "paper covers rock";
            final String ROCK_WINS = "rock smashes scissors";
            final String SCISSORS_WINS = "scissors cut paper";
            final String TIE = " you chose the same as the computer";

            // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
            if (playerChoice == computerChoice) {
                return TIE;
            } else if (userWins(playerChoice, computerChoice)) {
                if (playerChoice == 1) {
                    return "The player wins because " + ROCK_WINS+".";
                } else if (playerChoice == 2) {
                    return "The player wins because " +SCISSORS_WINS +".";
                } else {
                    return "The player wins because " +PAPER_WINS +".";
                }
            } else {
                if (computerChoice == 1) {
                    return "The computer wins because " + ROCK_WINS +".";
                } else if (computerChoice == 2) {
                    return "The computer wins because " + SCISSORS_WINS + ".";
                } else {
                    return "The computer wins because "+ PAPER_WINS + ".";
                }
            }
            //return null;
        }

        /**
         * Program entry point. Do not edit.
         */
        public static void main (String[]args){

            RockPaperScissors ex = new RockPaperScissors();
            ex.start();

        }
    }
